package windowbuilder;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Testfenster extends JFrame {

	private JPanel pnl_Hintergrund;
	private JTextField tfd_Eingabe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Testfenster frame = new Testfenster();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Testfenster() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 317, 553);
		pnl_Hintergrund = new JPanel();
		pnl_Hintergrund.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnl_Hintergrund);
		pnl_Hintergrund.setLayout(null);
		
		JTextField lbl_Ausgabe = new JTextField();
		lbl_Ausgabe.setText("Dieser Text soll ver\u00E4ndert werden.");
		lbl_Ausgabe.setBounds(10, 11, 281, 68);
		lbl_Ausgabe.setOpaque(false);
		lbl_Ausgabe.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		pnl_Hintergrund.add(lbl_Ausgabe);
		
		JButton btn_hintergrund_rot = new JButton("Rot");
		btn_hintergrund_rot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnl_Hintergrund.setBackground(Color.RED);
			}
		});
		btn_hintergrund_rot.setBounds(10, 121, 89, 23);
		pnl_Hintergrund.add(btn_hintergrund_rot);
		
		JButton btn_hintergrund_grün = new JButton("Gr\u00FCn");
		btn_hintergrund_grün.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnl_Hintergrund.setBackground(Color.GREEN);
			}
		});
		btn_hintergrund_grün.setBounds(105, 121, 89, 23);
		pnl_Hintergrund.add(btn_hintergrund_grün);
		
		JButton btn_hintergrund_blau = new JButton("Blau");
		btn_hintergrund_blau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnl_Hintergrund.setBackground(Color.BLUE);
			}
		});
		btn_hintergrund_blau.setBounds(204, 121, 89, 23);
		pnl_Hintergrund.add(btn_hintergrund_blau);
		
		JButton btn_hintergrund_gelb = new JButton("Gelb");
		btn_hintergrund_gelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnl_Hintergrund.setBackground(Color.YELLOW);
			}
		});
		btn_hintergrund_gelb.setBounds(10, 155, 89, 23);
		pnl_Hintergrund.add(btn_hintergrund_gelb);
		
		JButton btn_hintergrund_standard = new JButton("Standardfarbe");
		btn_hintergrund_standard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnl_Hintergrund.setBackground(new Color(0xEEEEEE));
			}
		});
		btn_hintergrund_standard.setBounds(105, 155, 89, 23);
		pnl_Hintergrund.add(btn_hintergrund_standard);
		
		JButton btn_hintergrund_frei = new JButton("Farbe w\u00E4hlen");
		btn_hintergrund_frei.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		btn_hintergrund_frei.setBounds(204, 155, 89, 23);
		pnl_Hintergrund.add(btn_hintergrund_frei);
		
		JButton btn_schriftart_arial = new JButton("Arial");
		btn_schriftart_arial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_Ausgabe.setFont(new Font("Arial", Font.PLAIN, 12));
			}
		});
		btn_schriftart_arial.setBounds(10, 210, 89, 23);
		pnl_Hintergrund.add(btn_schriftart_arial);
		
		JButton btn_schriftart_comicsans = new JButton("Comic Sans MS");
		btn_schriftart_comicsans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_Ausgabe.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
			}
		});
		btn_schriftart_comicsans.setBounds(105, 210, 89, 23);
		pnl_Hintergrund.add(btn_schriftart_comicsans);
		
		JButton btn_schriftart_courier = new JButton("Courier New");
		btn_schriftart_courier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_Ausgabe.setFont(new Font("Courier New", Font.PLAIN, 12));
			}
		});
		btn_schriftart_courier.setBounds(204, 210, 89, 23);
		pnl_Hintergrund.add(btn_schriftart_courier);
		
		tfd_Eingabe = new JTextField();
		tfd_Eingabe.setText("Hier bitte Text eingeben");
		tfd_Eingabe.setBounds(10, 244, 280, 20);
		pnl_Hintergrund.add(tfd_Eingabe);
		tfd_Eingabe.setColumns(10);
		
		JButton btn_insLabelschreiben = new JButton("Ins Label schreiben");
		btn_insLabelschreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_Ausgabe.setText(tfd_Eingabe.getText());
			}
		});
		btn_insLabelschreiben.setBounds(10, 275, 135, 23);
		pnl_Hintergrund.add(btn_insLabelschreiben);
		
		JButton btn_imLabellöschen = new JButton("Text im Label l\u00F6schen");
		btn_imLabellöschen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_Ausgabe.setText("");
			}
		});
		btn_imLabellöschen.setBounds(155, 275, 135, 23);
		pnl_Hintergrund.add(btn_imLabellöschen);
		
		JButton btn_schriftfarbe_rot = new JButton("Rot");
		btn_schriftfarbe_rot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_Ausgabe.setForeground(Color.RED);
			}
		});
		btn_schriftfarbe_rot.setBounds(10, 332, 89, 23);
		pnl_Hintergrund.add(btn_schriftfarbe_rot);
		
		JButton btn_schriftfarbe_blau = new JButton("Blau");
		btn_schriftfarbe_blau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_Ausgabe.setForeground(Color.BLUE);
			}
		});
		btn_schriftfarbe_blau.setBounds(105, 332, 89, 23);
		pnl_Hintergrund.add(btn_schriftfarbe_blau);
		
		JButton btn_schriftfarbe_schwarz = new JButton("Schwarz");
		btn_schriftfarbe_schwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_Ausgabe.setForeground(Color.BLACK);
			}
		});
		btn_schriftfarbe_schwarz.setBounds(204, 332, 89, 23);
		pnl_Hintergrund.add(btn_schriftfarbe_schwarz);
		
		JButton btn_schriftgröße_größer = new JButton("+");
		btn_schriftgröße_größer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int groesse = lbl_Ausgabe.getFont().getSize();
				lbl_Ausgabe.setFont(new Font(lbl_Ausgabe.getFont().getFontName(), Font.PLAIN, groesse + 1));
			}
		});
		btn_schriftgröße_größer.setBounds(10, 386, 135, 23);
		pnl_Hintergrund.add(btn_schriftgröße_größer);
		
		JButton btn_schriftgröße_kleiner = new JButton("-");
		btn_schriftgröße_kleiner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int groesse = lbl_Ausgabe.getFont().getSize();
				lbl_Ausgabe.setFont(new Font(lbl_Ausgabe.getFont().getFontName(), Font.PLAIN, groesse - 1));
			}
		});
		btn_schriftgröße_kleiner.setBounds(155, 386, 135, 23);
		pnl_Hintergrund.add(btn_schriftgröße_kleiner);
		
		JButton btn_ausrichtung_links = new JButton("linksb\u00FCndig");
		btn_ausrichtung_links.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_Ausgabe.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btn_ausrichtung_links.setBounds(10, 442, 89, 23);
		pnl_Hintergrund.add(btn_ausrichtung_links);
		
		JButton btn_ausrichtung_zentriert = new JButton("zentriert");
		btn_ausrichtung_zentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_Ausgabe.setHorizontalAlignment(SwingConstants.CENTER);	
			}
		});
		btn_ausrichtung_zentriert.setBounds(105, 442, 89, 23);
		pnl_Hintergrund.add(btn_ausrichtung_zentriert);
		
		JButton btn_ausrichtung_rechts = new JButton("rechtsb\u00FCndig");
		btn_ausrichtung_rechts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lbl_Ausgabe.setHorizontalAlignment(SwingConstants.RIGHT);	
			}
		});
		btn_ausrichtung_rechts.setBounds(204, 442, 89, 23);
		pnl_Hintergrund.add(btn_ausrichtung_rechts);
		
		JButton btn_exit = new JButton("EXIT");
		btn_exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(1);
			}
		});
		btn_exit.setBounds(10, 491, 281, 23);
		pnl_Hintergrund.add(btn_exit);
		
		JTextPane lbl_Beenden = new JTextPane();
		lbl_Beenden.setText("Aufgabe 6: Programm beenden");
		lbl_Beenden.setBounds(10, 471, 281, 20);
		lbl_Beenden.setOpaque(false);
		pnl_Hintergrund.add(lbl_Beenden);
		
		JTextPane lbl_Ausrichtung = new JTextPane();
		lbl_Ausrichtung.setText("Aufgabe 5: Textausrichtung");
		lbl_Ausrichtung.setBounds(10, 411, 281, 20);
		lbl_Ausrichtung.setOpaque(false);
		pnl_Hintergrund.add(lbl_Ausrichtung);
		
		JTextPane lbl_Schriftgröße = new JTextPane();
		lbl_Schriftgröße.setText("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		lbl_Schriftgröße.setBounds(10, 366, 281, 20);
		lbl_Schriftgröße.setOpaque(false);
		pnl_Hintergrund.add(lbl_Schriftgröße);
		
		JTextPane lbl_Schriftfarbe = new JTextPane();
		lbl_Schriftfarbe.setText("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lbl_Schriftfarbe.setBounds(10, 301, 281, 20);
		lbl_Schriftfarbe.setOpaque(false);
		pnl_Hintergrund.add(lbl_Schriftfarbe);
		
		JTextPane lbl_Format = new JTextPane();
		lbl_Format.setText("Aufgabe 2: Text formatieren");
		lbl_Format.setBounds(10, 179, 281, 20);
		lbl_Format.setOpaque(false);
		pnl_Hintergrund.add(lbl_Format);
		
		JTextPane lbl_Hintergrund = new JTextPane();
		lbl_Hintergrund.setText("Aufgabe 1: Hintergrund");
		lbl_Hintergrund.setBounds(10, 90, 281, 20);
		lbl_Hintergrund.setOpaque(false);
		pnl_Hintergrund.add(lbl_Hintergrund);
		
		
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
